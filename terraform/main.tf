resource "aws_s3_bucket" "bucket" {
  bucket = var.aws_s3_bucket

  versioning {
    enabled = true
  }

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}
 

resource "aws_s3_bucket_acl" "access_policy" {
  bucket = aws_s3_bucket.bucket.id
  acl    = "private"
}
